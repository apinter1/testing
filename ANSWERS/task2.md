## TL;DR

Relevant links:

- [Task repo](https://gitlab.com/apinter1/testing)
- [Initial run](https://gitlab.com/apinter1/testing/-/pipelines/933034682) - failed since git clone was not configured, since initial run it took a longer time
- [Fixed pipeline](https://gitlab.com/apinter1/testing/-/pipelines/933049787) - second run, image is already present on the node, execution time is much faster, demo of git clone
- [MR job test](https://gitlab.com/apinter1/testing/-/pipelines/933051647) - only run the `final` job on MRs demo

## 2.2/1

To make the pipeline faster I would package all the dependencies in a container and use that as a base image for the build. This way the dependencies would be cached and the build would be faster.

- Pulling an over 200MB image during every run (assuming that the image is set to be pulled every time), then installing all the dependencies will make things difficult to scale.
- Installing all the _python_ dependencies during every build is also not ideal, especially with the dependencies that are included as it takes a considerable amount of time to do so.
- Considering security: Looking at the current version of `python:3.9` there are some vulnerabilities in the image, might worth to use a different image (possibly based on _Alpine_) and maintain the dependencies. This image could be built on a daily basis and pushed to a registry, include updating all the base OS packages. A daily build would make sure that the image has all the latest security patches from upstream which _Alpine_ is pretty good at releasing.
- The built image will still be quite large (that is why I store it in my own registry, was still 20m to upload, but that is due to my bandwidth limitations), storing it in a registry that is near the runner (Gitlab registry), or on the same network (self-hosted runner, caching allowed), or supported by a CDN (quay.io, docker.io, GCR, ACR etc.) would make the pull faster, and future jobs would run smoother.
- The method also allows for a better dependency management.
- Setting the python dependency versions in _requirements.txt_ would save some headache down the road as well. Can be updated as needed, and tested with newer versions.

With the above changes the job would look like this:

```yaml
format:
    stage: format
    script:
      - black --check --diff -l 100 .
```

The suggested Dockerfile would look like this using `Alpine` as a base image, which  then can be built and pushed to a registry with a pipeline from a separate project:

```bash
$ cat Dockerfile

FROM docker.io/alpine:latest

RUN apk update && apk upgrade && \ 
    apk add --no-cache python3 py3-pip git

COPY requirements.txt /tmp/requirements.txt
RUN python3 -m pip install -r /tmp/requirements.txt
```

- This image could be stored in a private or a public registry. In case of a private registry one must set the `DOCKER_AUTH_CONFIG` with the docker auth token (or username/password, but base64 encoded) in the Gitlab project settings. This way the runner will be able to pull the image from the registry. In case of a public registry the image can be pulled without any authentication.

```json
"auths": {
  "registry.adathor.com:5000": {
    "auth": "dXNlcm5hbWU6cGFzc3dvcmQ="
  }
}
```

2.2/1. Alternative

Packaging the dependencies in a container image can be avoided by caching the first job the requirements are installed in. I personally prefer containerizing the build environment as it allows more control.

```yaml
image: docker.io/python:3.9

stages:
  - format
  - test
  - final

format:
    artifacts:
      name: "$CI_PIPELINE_ID"
      paths:
        - build/apinter1/testing
      expire_in: 30 mins
    stage: format
    script:
      - export PIP_CACHE_DIR=$PWD/cache
      - black --check --diff -l 100 .

test:
    artifacts:
      name: "$CI_PIPELINE_ID"
      paths:
        - build/apinter1/testing
      expire_in: 30 mins
  stage: test
  script:
    - export PIP_CACHE_DIR=$PWD/cache
    - export PYTHONPATH="$PYTHONPATH:."
    - git clone <INSERT-SOME-PRIVATE-REPO-URL>
    - pytest tests/

final-stage:
    artifacts:
      name: "$CI_PIPELINE_ID"
      paths:
        - build/apinter1/testing
      expire_in: 30 mins
  stage: final
  script:
    - export PIP_CACHE_DIR=$PWD/cache
    - echo "About to be merged into main!"
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: always
      allow_failure: false
```

## 2.2/2

Pulling from private repositories - assuming that the repo is also stored on Gitlab - can be solved by creating a _deploy token_ (`project settings > repository > deploy tokens) and using that to authenticate. This way the token can be revoked at any time and it can be limited to only pull from the registry. Of course this could also be solved by using a _personal access token_ configured for a "service account" (SA) or with an ssh key configured for the SA. However, the deploy token is a leaner solution, limits access to a single project, and limits the privileges to read-only so in case of a token leak an advisory can only pull the project, but can't push back malicious code (zero trust).
This token should be stored in the project's CI/CD settings as a variable (used as masked).

The token I generated for my project is `yN44W9NXcj3amBNXTExs` with a username `builder`. I store the token only as `JUST_TEST_TOKEN`.

Using a deploy token token in the job would look like this (over https):

```yaml
test:
  stage: test
  script:
    - export PYTHONPATH="$PYTHONPATH:."
    - git clone https://builder:$JUST_TEST_TOKEN@gitlab.com/apinter1/just-a-test
    - pytest tests/

```

## 2.2/3

Controlling when a job or a stage is executed can be done by using `rules` (refrain from using `only` since it has been deprecated for a few releases now <https://docs.gitlab.com/ee/ci/yaml/index.html#only--except>)
The job would look like this:

```yaml
final-stage:
  stage: final
  script:
    - echo "About to be merged into main!"
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: always
      allow_failure: false
```
