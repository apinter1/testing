## Task 3

### TL;DR

- Environment setup with Kubernetes,
- Deploy the online backend to their respective environments with _ArgoCD_,
- Package and release the mobile applications with _Fastlane_,
- Use _Fastlane_ to release to the Play/App Store to distribute the mobile applications for dev, UAT, and live.

### Common structure (holds true for every work repository)

Assuming that the Gitlab workflow is utilized, the following structure is recommended:

![base topo](https://adathor.ap-south-1.linodeobjects.com/xund/xund_topo.png)

- main branch is always the release branch
- additional branches are created for each feature in development, these are the "dev" branches deployed to the dev environment
- each feature branch is merged into the main branch via a merge request (MR) containing the necessary test results.
  - missing/failed tests will result in a refused MR automatically
- the main branch is deployed to the _UAT_ environment for further testing
- the UAT environment is used for manual testing by the client
- on successful UAT, the main branch is tagged and deployed to the _live_ environment

#### Including release notes

A release would only be created when a _main_ branch commit gets tagged.
_GitlabCI's_ release feature supports the creation of release notes, and the creation of a release page, but it is not possible to include the release notes in the release page. To overcome this limitation, a custom _GitlabCI_ job can be created that creates the release page, and includes the release notes in the page as an `asset`. This can also be achieved by the `release-cli` tool.

To create a release and attach the test results the test job should the results, and the CI job id in separate files. In the Release job the artifacts will be pulled in automatically (`artifacts: true`).

Building on the example provided by Gitlab (https://docs.gitlab.com/ee/user/project/releases/release_cicd_examples.html), excluding the applications build.

```yaml
prepare_job:
  stage: prepare
  rules:
    - if: $CI_COMMIT_TAG
      when: always
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    - echo "$CI_JOB_ID" >> CI_JOB_ID.txt
    - echo "TAG=v$(cat VERSION)" >> variables.env
    - pytest > test_results.txt
  artifacts:
    name: "$CI_PIPELINE_ID"
    paths:
      - variables.env
      - CI_JOB_ID.txt
      - test_results.txt

release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: prepare_job
      artifacts: true
  rules:
    - if: $CI_COMMIT_TAG
      when: always
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    - echo "running release_job for $TAG"
  release:
    name: 'Release $TAG'
    description: 'Automated release $EXTRA_DESCRIPTION'
    tag_name: '$TAG'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: 'notes'
          url: 'https://gitlab.com/build-pipeline/-/jobs/`cat CI_JOB_ID.txt`/artifacts/file/test_results.txt\'
        - name: 'asset'
          url: '[https://example.com/assets/2](https://gitlab.com/build-pipeline/-/jobs/`cat CI_JOB_ID.txt`/artifacts/file/app.apk\)'
          filepath: '/pretty/url/1'
          link_type: 'other'
```

### Backend infrastructure

Assuming that the infrastructure is running a recent, supported version of Kubernetes (k8s).

#### Deployment of online components (backend)

Assuming that the `Webadmin` and `Data science` project are required here.

- the backend application is built and tested after a commit to the __feature branch__ and pushed to the container registry
- on a successful build, the feature branch is deployed to the __dev__ environment for internal testing purposes
- on a successful test, the feature branch is merged into the __main__ branch, and deployed to the __UAT__ environment for client testing
- on a successful __UAT__ test, the main branch is tagged and deployed to the __live__ environment

The deployment is carried out by _ArgoCD_ in every case, by changing the image tag in the deployment manifest stored in the _Gitops_ repository for each environments. Separating the deployment manifests from the working code allows for a more flexible deployment process, and allows for a more granular control over the deployment process, and ease the rollback as well. _ArgoCD_ has the ability to refuse deployment in case the manifest is malformed, or the image tag is invalid leaving the environment in a consistent state. Introducing ChatOps can further improve the process by allowing the system to notify Dev and DevOps of a successful/failed deployment.

To allow for _ArgoCD_ to deploy the application requires a _helm chart_ that includes all the required _k8s_ manifests (deployment, service, ingress, etc.) and a _values.yaml_ file that contains the environment specific configuration. The _values.yaml_ file should be stored in the _Gitops_ repository, and the _helm chart_ should be stored in the application's repository. This way the _helm chart_ can be versioned with the application, and the _values.yaml_ can be versioned with the environment.

Deployment is triggered by changing the image tag(s) in the _Gitops_ project by the relevant pipeline (_Webadmin_ or _Data science_). 

_NOTE:_ This is just one of the many ways to deploy the application, but one of the easiest, and most feature rich. An alternative would be to deploy with _Ansible_ storing each play execution in _ARA_ by setting up the required callback functions, or using a Gitlab runner to run Helm directly in the cluster, excluding the additional _Gitops_ and _Helm chart_ repositories. I personally prefer ArgoCD since it is a dedicated tool for this purpose, and it is easy to set up and use offering a lot of out-of-the box features.

_NOTE2:_ ArgoCD in this example is not exposed to the internet, and is only accessible from the within the cluster. Deployment can only be changed via the _Gitops_ project.

_NOTE3:_ In addition to the above, I would scan the produced image for security vulnerabilities with `trviy` for example, and reporting back issues to the developer. Code scanning could be done by Gitlab's built-in security scanning tool (if available) or by a dedicated tool like `SonarQube` or `Chekov`.

### Android and iOS applications

There are two methods to consider here:

- build a custom containerized environment for the task with `fastlane`,
- use Gitlab's Mobile DevOps (still experimental, but essentially removes the burden of maintaining a custom environment).

As it is challenging to maintain a custom tool with `fastlane` for the task I would personally opt for Gitlab's Mobile DevOps.

Either way the application can be built and tested in a similar way to the backend application, and deployed to the relevant environment. The only difference is that the application is not deployed to the environment directly, but to the relevant app store. This can be achieved by using `fastlane`'s `deliver` action, or by using Gitlab's Mobile DevOps. This can be delivered to the client via TestFlight, or Google Play's internal testing feature, can be made available to the client via a private link, or can be made available to the public via the app store beta release features.

#### Thoughts on application testing

There are two main types of testing to consider here:

- unit testing,
- UI testing.

Unit testing can be carried out by the developer, and can be integrated into the CI/CD pipeline. UI testing is a bit more challenging, and requires a dedicated tool or an emulator. There are a few options here:

- use a dedicated tool like `Appium`,
- use a dedicated service like `Firebase Test Lab`,
- use Gitlab's Mobile DevOps (here again it essentially removes the burden of maintaining a custom environment or utilizing another service outside of the _Gitlab_ environment).
