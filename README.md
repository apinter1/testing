# DevOps Engineer Assignment

## Problems with current project
- Installing dependencies from `requirements.txt` multiple times per pipeline w/o caching
- Duplicate code in CI
- Final stage of CI always runs
- Cannot clone private packages
- Dependencies don't have versions


## Task files

You should send the following files/folders to the applicant:

- `src/`
- `tests/`
- `.gitlab-ci.yml`
- `requirements.txt`