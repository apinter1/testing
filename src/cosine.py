import torch


def cosine_similarity(x, y):
    x_norm = x / torch.linalg.norm(x, dim=1)
    y_norm = y / torch.linalg.norm(y, dim=1)
    cossim = torch.matmul(x_norm, y_norm.T)
    return cossim
