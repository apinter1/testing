import torch
from src.cosine import cosine_similarity
from torch.testing import assert_close


def test_cosine_similarity():
    x = torch.tensor([[0.5, 0.8660]])
    y = torch.tensor([[1.0, 0.0]])
    assert_close(cosine_similarity(x, y), torch.tensor([[0.5]]), rtol=0.1, atol=0.01)
